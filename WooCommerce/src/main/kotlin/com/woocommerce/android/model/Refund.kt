package com.woocommerce.android.model

import org.wordpress.android.fluxc.model.refunds.WCRefundModel
import org.wordpress.android.fluxc.model.refunds.WCRefundModel.WCRefundItem

typealias Refund = WCRefundModel
typealias Item = WCRefundItem
